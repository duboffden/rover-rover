
var express = require('express');
var exec = require('child_process').exec;
var app = express();
var http = require('http').Server(app);

function takePhoto(){
    console.log("attempt to take photo;");
    exec("fswebcam /home/pi/node/photos/photo.jpg", function (error, stdout, stderr) {
        if (error !== null) {
            console.log('exec error: ' + error);
        }
        console.log("taken");
        setTimeout(takePhoto, 2000);
    });
}


takePhoto();
app.use('/photos', express.static('photos'));

http.listen(9346, function(){
    console.log('listening on *:9346');
});
