
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.use('/images', express.static('images'));
app.use('/data', express.static('data'));
app.use('/socket.io', express.static('socket.io'));
app.use("/index.html", function(req, res){
    res.sendFile(__dirname + "/index.html");
});
app.use("/backgroundpattern.png", function(req, res){
    res.sendFile(__dirname + "/backgroundpattern.png");
});
var playerId = 0;
var playersPositions = [];

io.on('connection', function(socket){
    var id = playerId++;
    playersPositions[id] = {"id" : id, "x" : 200, "y": 200};
    socket.broadcast.emit("newPlayer" , playersPositions[id]);
    for ( var i = 0; i < id; i++){
        socket.emit('newPlayer', playersPositions[i]);
    }
    socket.on("respawn", function(){
        playersPositions[id] =  {"id" : id, "x" : 200, "y": 200};
        socket.broadcast.emit("newPlayer" , playersPositions[id]);
    });
    socket.emit('yourID', id);
    socket.on('playerMove', function(msg){
        playersPositions[id] = {"id" : id, "x" : msg[0], "y": msg[1]};
        socket.broadcast.emit('playerMove', playersPositions[id]);
    });
    socket.on("disconnect", function(){
        if(playersPositions[id] == null) return;
        io.emit("disconnect", id)
        playersPositions[id] = null;
    });
    socket.on("destroy", function(msg){
        io.emit("destroy", msg);
        playersPositions[msg] = null;
    });
    socket.on("fire", function(msg){
        socket.broadcast.emit("fire", {id: id, angle1: msg.angle1});
    });
    socket.on("message",function(msg){
        socket.broadcast.emit("Message", msg)
    });
});

http.listen(9346, function(){
    console.log('listening on *:9345');
});
