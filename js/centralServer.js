var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');
var compression = require('compression');
var formidable = require('formidable');
var path = require('path');
var fs = require('fs-extra');

var app = express();

app.use(compression());
app.use(bodyParser.urlencoded({extended: false}));

var isUploadingImage = false;

app.use('/rover_photos', express.static('rover_photos'));

app.post("/upload", function (req, res) {

    if (isUploadingImage) {
        res.end('error');
        return;
    }
    isUploadingImage = true;
    console.log("uploading file");

    var form = new formidable.IncomingForm();

    form.on('file', function (field, file) {
        fs.rename(file.path, path.join(form.uploadDir, "photo.jpg"));
        console.log("file uploaded" + "file size = " + file.size);
        setTimeout(showUploadedPhoto, file.size/5);
    });

    // log any errors that occur
    form.on('error', function (err) {
        console.log('An error has occured: \n' + err);
    });

    form.on('end', function () {
        res.end('success');
    });
    // store all uploads in the /uploads directory
    form.uploadDir = path.join(__dirname, '/uploads');
    // once all the files have been uploaded, send a response to the client

    // parse the incoming request containing the form data
    form.parse(req);
})

function showUploadedPhoto() {
    try {
        fs.copySync(path.resolve(__dirname, './uploads/photo.jpg'), path.resolve(__dirname, './rover_photos/photo.jpg'))
        isUploadingImage = false;
        console.log("success!")
    } catch (err) {
        console.error(err)
    }
}

var httpProxy = require('http-proxy');
var proxy = httpProxy.createProxyServer({
    target: 'http://91.202.135.104:8080'
});

var proxyServer = http.createServer(function(req, res) {
    proxy.web(req, res);
});

proxyServer.on('upgrade', function (req, socket, head) {
    proxy.ws(req, socket, head);
});

proxyServer.listen(5050);

http.createServer(app).listen(9346, function () {
    console.log('listening on *:9346');
});
